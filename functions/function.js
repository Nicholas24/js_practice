let user = {
    name: "Nicholas",
    age: 23
}

// Constructor Functions
function User() {
    this.name = user.name;
    this.isAdmin = false;
}

let nicholas = new User();

// Arrow function example
let sayHi = () => {
    alert(user.name + " " + "says Hi")
};

// Function Example
function sayBye() {
    alert(user.name + " says Bye");
}



sayHi();
sayBye();
alert(nicholas.name);
alert(nicholas.isAdmin);