class Oracle_Apex {
    constructor(workspace) {
        this.workspace = workspace;
    }

    project(name) {
        alert("Name of project is: " + name + " on workspace " + this.workspace);
    }
}

class Workspace extends Oracle_Apex {

    getName() {
        console.log(this.workspace);
    }
}


let apex = new Workspace("GAU");
apex.project("Test");
apex.getName();