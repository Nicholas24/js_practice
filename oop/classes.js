class User {
    constructor(name) {
        this.name = name;
    }

    sayHello() {
        alert(this.name + " says hello");
    }
}


let nicholas = new User("Nicholas");
nicholas.sayHello();